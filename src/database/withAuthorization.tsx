import * as React from "react";
import { withRouter, Redirect } from "react-router-dom";
import { AuthUserContext } from "./AuthUserContext";

interface InterfaceProps {
  history?: any;
}

export const withAuthorization = (condition: any) => (Component: any) => {
  class WithAuthorization extends React.Component<InterfaceProps, {}> {
    state = {
      authUser: null
    };

    public render() {
      const contextType = AuthUserContext;

      return contextType ? (
        <React.Fragment>
          <Component {...this.props} {...this.state} />
        </React.Fragment>
      ) : (
        <Redirect to="/" />
      );
    }
  }

  return withRouter(WithAuthorization as any);
};
