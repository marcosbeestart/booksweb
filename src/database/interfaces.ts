export interface ComboBoxOption {
  value: string;
  label: string;
}

// Category of book, like 'Pousada', 'Pesqueiro', 'etc...'
export interface Category extends ComboBoxOption {}

// Bait types, like 'Iscas artificiais', 'Iscas naturais', 'etc...'
export interface BaitType extends ComboBoxOption {}

// Fishing spots, like 'Mangue', 'Lago', 'etc...'
export interface FishingSpot extends ComboBoxOption {}

// Brazil states, ex: { label: 'Distrito Federal', value: 'DF', cities: ['Brasília'] }
export interface State {
  value: number;
  label: string;
  nome: string;
}

// Brazil cities, ex: { label: 'Adamantina', value: 'Adamantina' }
export interface City extends ComboBoxOption {}

// Plans
export interface Plan extends ComboBoxOption {}
