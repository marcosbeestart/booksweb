import { BookData } from "../modules/books/screens/interfaces";

export const searchBooks = async (searchValue: string) => {
  return [] as BookData[];
};

export const getApi = () => {
  return process.env.NODE_ENV === "development"
    ? "http://localhost:5000"
    : "https://books-282000.uc.r.appspot.com";
};

interface RequestParams {
  params: string;
  method: "GET" | "POST" | "PUT" | "PATCH" | "DELETE";
  headers?: any;
  body?: {};
}

export const requestAPI = async ({
  params,
  method,
  body,
  headers = {}
}: RequestParams) => {
  //   const token = await auth.currentUser.getIdToken();
  const url = `${getApi()}${params}`;
  console.log("url: ", url);
  console.log("body: ", JSON.stringify(body));
  const response = await fetch(url, {
    method,
    headers: {
      Authorization: "",
      "Content-Type": "application/json",
      ...headers
    },
    body: body ? JSON.stringify(body) : null
  });

  return await response.json();
};
