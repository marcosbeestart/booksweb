import { requestAPI } from "../services";
import Toast from "../../components/Toast/Toast";
import { BookData } from "../../modules/books/screens/interfaces";

/**
 * Update book data
 * @param id string -> id of doc
 * @param data
 */

export const updateBook = async (id: string, data: BookData) => {
  try {
    await requestAPI({
      params: `/books/${id}`,
      method: "PUT",
      body: data
    });

    return { success: true };
  } catch (e) {
    console.log("updateBook error", e);
    return e;
  }
};

/**
 * Create new book
 * @param data
 */

export const createBook = async (data: BookData) => {
  try {
    const response = await requestAPI({
      params: `/books/`,
      method: "POST",
      body: data
    });
    return response;
  } catch (e) {
    console.log("saveNewUser error:", e);
    return e;
  }
};

/**
 * Delete a specific book
 * @param id string -> id of book
 */

export const deleteBook = async (id: string) => {
  try {
    const response = await requestAPI({
      params: `/books/${id}`,
      method: "DELETE"
    });

    return response;
  } catch (error) {
    console.log("deleteBook error: ", error);
    Toast("error", "OOPS! Erro no servidor. Tente novamente.");
  }
};

/**
 * Return specific book
 * @param id string -> id of book
 */

export const getBook = async (id: string) => {
  let books = await requestAPI({
    params: `/books/${id}`,
    method: "GET"
  });

  return books;
};

/**
 * Return all the books
 */

export async function getAllBooks() {
  let books = await requestAPI({
    params: `/books/`,
    method: "GET"
  });

  return books;
}
