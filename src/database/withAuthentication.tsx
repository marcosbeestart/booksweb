import React from "react";
import { AuthUserContext } from "./AuthUserContext";

interface InterfaceProps {
  authUser?: any;
  history?: any;
}

interface InterfaceState {
  authUser?: any;
}

export const withAuthentication = (Component: any) => {
  class WithAuthentication extends React.Component<
    InterfaceProps,
    InterfaceState
  > {
    constructor(props: any) {
      super(props);

      this.state = {
        authUser: null
      };
    }

    public render() {
      const { authUser } = this.state;

      return (
        <AuthUserContext.Provider value={authUser}>
          <Component {...this.state} />
        </AuthUserContext.Provider>
      );
    }
  }
  return WithAuthentication;
};
