import React from 'react';
import styled from 'styled-components';

interface STButtonProps {
  size: string;
  color: string;
  children: any;
  onClick?: () => void;
}

const StyledSimpleTextButton = styled.button<STButtonProps>`
  color: ${props => props.color};
  font-size: ${props => props.size};
  text-transform: uppercase;
  background: none;
  border: 0;
  display: inline-flex;
  align-items: center;
  outline: 0;
`;

export default function SimpleTextButton(props: STButtonProps): JSX.Element {
  return (
    <StyledSimpleTextButton
      onClick={props.onClick}
      color={props.color}
      size={props.size}
    >
      {props.children}
    </StyledSimpleTextButton>
  );
}
