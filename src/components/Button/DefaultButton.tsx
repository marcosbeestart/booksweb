import React from "react";
import { Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { WHITE, CERULEAN, MYSTIC } from "../../utils/colors";
import styled from "styled-components";

const DefaultButton = withStyles({
  root: {
    color: WHITE,
    backgroundColor: CERULEAN,
    padding: "16px",
    lineHeight: "1.4",
    textTransform: "none",
    fontFamily: "Overpass",
    fontSize: "16px",
    boxSizing: "border-box",
    "&:hover": {
      color: WHITE,
      backgroundColor: CERULEAN
    }
  }
})(styled(({ ...props }): JSX.Element => <Button {...props} />)`
  && {
    width: ${(props) => props.width};
  }
`);

export const DefaultButtonOutlined = styled(
  ({ ...props }): JSX.Element => <DefaultButton {...props} />
)`
  && {
    background-color: transparent;
    color: ${CERULEAN};
    border: 1px solid ${(props) => props.colorborder || MYSTIC};
  }
`;

export default DefaultButton;
