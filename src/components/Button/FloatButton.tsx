import React from "react";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import { CERULEAN } from "../../utils/colors";
import './FloatButton.scss';

const useStyles = makeStyles(() =>
  createStyles({
    fab: {
      position: "fixed",
      bottom: 50,
      right: 50,
      color: "white",
      backgroundColor: CERULEAN,
      "&:hover": {
        backgroundColor: CERULEAN
      }
    }
  })
);

interface FloatButtonProps {
  onClick: () => void;
  children?: React.ReactNode;
}

FloatButton.defaultProps = {
  children: <AddIcon />
};

export default function FloatButton(props: FloatButtonProps): JSX.Element {
  const classes = useStyles();

  return (
    <Fab className={classes.fab} onClick={props.onClick}>
      +
    </Fab>
  );
}
