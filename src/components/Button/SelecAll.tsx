import React from 'react';
import Checkbox from '../Checkbox/DarkGrayCheckBox';
import './SelectAll.scss';

export default function SelectAll({
  onChange,
  children
}: {
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  children: any;
}): JSX.Element {
  return (
    <label className="select-all">
      <Checkbox
        onChange={onChange}
        inputProps={{ 'aria-label': 'Select all' }}
      />{' '}
      <span className="select-all__button">{children}</span>
    </label>
  );
}
