import React from 'react';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import styled from 'styled-components';
import { CERULEAN, SHUTTLE_GRAY } from '../../utils/colors';

interface DefaultBreadcrumbsProps {
  children: React.ReactNode;
}

const StyledBreadCrumb = styled(({ ...props }) => <Breadcrumbs {...props} />)`
  && {
    li {
      font-family: 'Overpass', sans-serif;
    }
    li > a,
    li > p {
      font-size: 12px;
      line-height: 1.5;
      display: block;
    }

    li > a {
      color: ${CERULEAN};
    }
    li > p {
      color: ${SHUTTLE_GRAY};
    }
  }
`;

export default function DefaultBreadcrumbs(
  props: DefaultBreadcrumbsProps
): JSX.Element {
  return (
    <StyledBreadCrumb
      separator={<NavigateNextIcon fontSize="small" />}
      aria-label="breadcrumb"
    >
      {props.children}
    </StyledBreadCrumb>
  );
}
