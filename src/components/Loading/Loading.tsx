import React from "react";
import Spinner from "react-spinkit";
import "./Loading.scss";
import { CERULEAN } from "../../utils/colors";

const Loading = ({
  loading,
  message
}: {
  loading: boolean;
  message?: string;
}) => {
  return loading ? (
    <div className="overlay-content">
      <div className="wrapper">
        <Spinner name="double-bounce" fadeIn="none" color={CERULEAN} />
        <span className="message">{message}</span>
      </div>
    </div>
  ) : null;
};

Loading.defaultProps = {
  message: "carregando..."
};

export default Loading;
