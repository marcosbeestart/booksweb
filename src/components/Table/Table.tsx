import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { withStyles, Theme, createStyles } from "@material-ui/core/styles";
import { BLACK_HAZE, DAINTREE, OSLO_GRAY } from "../../utils/colors";
import "./Table.scss";

export const StyledTable = withStyles({
  root: {
    borderCollapse: "separate",
    borderSpacing: "0 8px"
  }
})(Table);

export const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    body: {
      fontSize: 14,
      borderWidth: 0,
      backgroundColor: BLACK_HAZE,
      color: DAINTREE,
      padding: "6px",
      lineHeight: 1,
      fontFamily: "Overpass"
    }
  })
)(TableCell);

export const StyledTableCellHead = withStyles({
  root: {
    color: OSLO_GRAY,
    textTransform: "uppercase",
    fontWeight: 300,
    borderBottom: 0,
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 5,
    fontFamily: "Overpass"
  }
})(TableCell);

export const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      "& td:first-child": {
        borderTopLeftRadius: "6px",
        borderBottomLeftRadius: "6px"
      },
      "& td:last-child": {
        borderTopRightRadius: "6px",
        borderBottomRightRadius: "6px"
      }
    }
  })
)(TableRow);

export const StyledTableHead = withStyles((theme: Theme) =>
  createStyles({
    root: {}
  })
)(TableHead);

export const StyledTableBody = withStyles((theme: Theme) =>
  createStyles({
    root: {}
  })
)(TableBody);
