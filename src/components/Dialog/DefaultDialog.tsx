import React from 'react';
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles
} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { OSLO_GRAY } from '../../utils/colors';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: 0
    },
    typography: {
      fontFamily: 'Overpass',
      fontSize: '24px',
      fontWeight: 'bold',
      color: OSLO_GRAY
    },
    closeButton: {
      position: 'absolute',
      right: 40,
      top: 30,
      color: theme.palette.grey[500]
    },
    dialogPaper: {
      minHeight: '80vh',
      maxHeight: '80vh',
      height: '100%',
      padding: '40px 40px 0',
      maxWidth: '355px'
    }
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6" className={classes.typography}>
        {children}
      </Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <img src="/assets/icon-x.svg" alt="" />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: 0,
    height: 'calc(100% - 216px)',
    marginTop: 40,
    marginBottom: 40
  }
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: 0,
    width: '100%',
    boxSizing: 'border-box'
  }
}))(MuiDialogActions);

export interface DialogProps extends WithStyles<typeof styles> {
  title: string;
  children: React.ReactNode;
  actionContent?: React.ReactNode;
}

const CustomizedDialogs = withStyles(styles)((props: DialogProps) => {
  const [open, setOpen] = React.useState(true);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog
        classes={{ paper: props.classes.dialogPaper }}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        fullWidth
        scroll={'body'}
        maxWidth={'xs'}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {props.title}
        </DialogTitle>
        <DialogContent>{props.children}</DialogContent>
        <DialogActions>{props.actionContent || null}</DialogActions>
      </Dialog>
    </div>
  );
});

export default CustomizedDialogs;
