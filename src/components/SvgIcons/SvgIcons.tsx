import React from 'react';
import styled from 'styled-components';
import SvgIcon from '@material-ui/core/SvgIcon';

export interface CustomSVGIcons {
  iconcolor: string;
  viewBox?: string;
  size: string;
}

export const StyledIcon = styled(
  ({ ...props }): JSX.Element => <SvgIcon {...props} />
)`
  && {
    color: ${props => props.iconcolor};
    font-size: ${props => props.size};
  }
`;
