import React from 'react';
import { Checkbox, CheckboxProps } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { IRON, CERULEAN } from '../../utils/colors';

const DefaultCheckbox = withStyles({
  root: {
    color: IRON,
    '&$checked': {
      color: CERULEAN
    }
  },
  checked: {}
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

export default DefaultCheckbox;
