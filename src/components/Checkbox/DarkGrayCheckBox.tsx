import React from 'react';
import { Checkbox, CheckboxProps } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { SHUTTLE_GRAY, CERULEAN } from '../../utils/colors';

const DarkGrayCheckBox = withStyles({
  root: {
    color: SHUTTLE_GRAY,
    '&$checked': {
      color: CERULEAN
    }
  },
  checked: {}
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

export default DarkGrayCheckBox;
