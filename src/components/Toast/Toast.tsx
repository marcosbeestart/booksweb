import React from "react";
import PropTypes from "prop-types";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./Toast.scss";

const Toast = (type: string, message: string) => {
  return toast(
    <div className="toast-wrapper">
      <img
        className="toast-icon"
        src={`/assets/icon-${type}-toast.svg`}
        alt=""
      />
      <span className="toast-message">{message}</span>
    </div>,
    {
      className: `toaster--${type}`,
      type: toast.TYPE.INFO,
      autoClose: 3500
    }
  );
};

Toast.propTypes = {
  type: PropTypes.oneOf(["success", "error", "warning", "info"]).isRequired,
  message: PropTypes.string.isRequired
};

export default Toast;
