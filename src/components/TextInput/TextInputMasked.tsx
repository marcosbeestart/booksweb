import React from 'react';
import {
  OutlinedInput,
  OutlinedInputProps,
  InputLabel
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { IRON, SHUTTLE_GRAY } from '../../utils/colors';
import { FormErrors } from '../../utils/FormErrors';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import InputMask from 'react-input-mask';

interface TextInputMaskedProps extends OutlinedInputProps {
  label?: string;
  placeholder?: string;
  fullWidth?: boolean;
  value?: string;
  errors?: FormErrors[];
  name?: string;
  mask: string;
  getValueToParent: (value: any) => void;
}

const StyleInputLabel = withStyles({
  root: {
    color: 'black',
    lineHeight: '1.6em',
    marginTop: 8,
    marginBottom: 8,
    fontFamily: 'Overpass',
    fontWeight: 300
  }
})(InputLabel);

const DefaultOutlinedInput = withStyles({
  root: {
    fontFamily: 'Overpass',
    '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
      borderColor: IRON
    },
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: IRON
    }
  },
  input: {
    color: SHUTTLE_GRAY
  }
})(OutlinedInput);

const useStyles = makeStyles(() =>
  createStyles({
    inpurError: {
      border: '1px solid red'
    },
    labelError: {
      color: 'red'
    }
  })
);

const TextInputMasked = ({
  label,
  placeholder,
  labelWidth,
  fullWidth,
  errors,
  name,
  mask,
  value,
  getValueToParent,
  ...rest
}: TextInputMaskedProps): JSX.Element => {
  const [fieldValue, setValue] = React.useState(value);
  const classes = useStyles();
  const field =
    errors !== undefined && errors.length > 0
      ? errors.filter((e: FormErrors) => e.path === name)
      : null;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
    getValueToParent(event.target.value);
  };

  return (
    <>
      {label && <StyleInputLabel>{label}</StyleInputLabel>}
      <InputMask
        mask={mask}
        maskChar={'\u2000'}
        value={fieldValue}
        onChange={handleChange}
        placeholder={placeholder}
      >
        {(inputProps: any) => (
          <DefaultOutlinedInput
            fullWidth={fullWidth}
            labelWidth={labelWidth}
            placeholder={placeholder}
            name={name}
            className={field && field[0] ? classes.inpurError : ''}
            {...inputProps}
            {...rest}
          />
        )}
      </InputMask>

      {field && field[0] && (
        <label className={classes.labelError}>{field[0].errors}</label>
      )}
    </>
  );
};

TextInputMasked.defaultProps = {
  labelWidth: 0,
  value: '',
  getValueToParent: () => {}
};

export default TextInputMasked;
