import React from 'react';
import {
  OutlinedInput,
  OutlinedInputProps,
  InputLabel,
  InputAdornment,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { IRON, SHUTTLE_GRAY } from '../../utils/colors';
import { FormErrors } from '../../utils/FormErrors';
import { makeStyles, createStyles } from '@material-ui/core/styles';

interface TextInputProps extends OutlinedInputProps {
  label?: string;
  placeholder?: string;
  fullWidth?: boolean;
  value?: string;
  errors?: FormErrors[];
  name?: string;
  startAdornment?: string;
  getValueToParent: (value: any) => void;
}

const StyleInputLabel = withStyles({
  root: {
    color: 'black',
    lineHeight: '1.6em',
    marginTop: 8,
    marginBottom: 8,
    fontFamily: 'Overpass',
    fontWeight: 300
  }
})(InputLabel);

const DefaultOutlinedInput = withStyles({
  root: {
    fontFamily: 'Overpass',
    '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
      borderColor: IRON
    },
    '&:hover .MuiOutlinedInput-notchedOutline': {
      borderColor: IRON
    }
  },
  input: {
    color: SHUTTLE_GRAY
  }
})(OutlinedInput);

const useStyles = makeStyles(() =>
  createStyles({
    inpurError: {
      border: '1px solid red'
    },
    labelError: {
      color: 'red'
    }
  })
);

const TextInput = ({
  label,
  placeholder,
  labelWidth,
  fullWidth,
  errors,
  name,
  value,
  getValueToParent,
  startAdornment,
  ...rest
}: TextInputProps): JSX.Element => {
  const [fieldValue, setValue] = React.useState(value);
  const classes = useStyles();
  const field =
    errors !== undefined && errors.length > 0
      ? errors.filter((e: FormErrors) => e.path === name)
      : null;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
    getValueToParent(event.target.value);
  };

  return (
    <>
      {label && <StyleInputLabel>{label}</StyleInputLabel>}
      <DefaultOutlinedInput
        fullWidth={fullWidth}
        labelWidth={labelWidth}
        placeholder={placeholder}
        name={name}
        onChange={handleChange}
        className={field && field[0] ? classes.inpurError : ''}
        value={fieldValue}
        startAdornment={
          startAdornment && (
            <InputAdornment position="start">{startAdornment}</InputAdornment>
          )
        }
        {...rest}
      />
      {field && field[0] && (
        <label className={classes.labelError}>{field[0].errors}</label>
      )}
    </>
  );
};

TextInput.defaultProps = {
  labelWidth: 0,
  value: '',
  getValueToParent: () => {}
};

export default TextInput;
