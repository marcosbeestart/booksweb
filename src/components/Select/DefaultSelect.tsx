import React, { useEffect } from "react";
import { Select, InputLabel, FormControl, MenuItem } from "@material-ui/core";
import { withStyles, makeStyles, createStyles } from "@material-ui/core/styles";
import { IRON, CERULEAN, SHUTTLE_GRAY } from "../../utils/colors";
import { FormErrors } from "../../utils/FormErrors";
import "./DefaultSelect.scss";

interface SelecItems {
  value: string | number;
  label: string;
}

interface DefaultSelectProps {
  selectItems: SelecItems[];
  label?: string;
  placeholder?: string;
  name?: string;
  errors?: FormErrors[];
  value?: any;
  borderless?: boolean;
  fullWidth: boolean;
  getValueToParent: (value: any) => void;
}

const StyleMenuItem = withStyles({
  root: {
    fontFamily: "Overpass",
    color: SHUTTLE_GRAY,
    fontWeight: 200
  }
})(MenuItem);

const StyledFormControl = withStyles({
  root: {
    borderColor: IRON,

    "& .MuiFormLabel-root.Mui-focused": {
      color: SHUTTLE_GRAY
    },
    "& >.MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline": {
      borderColor: IRON
    },
    "& >.MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
      borderColor: IRON,
      fontFamily: "Overpass"
    },
    "& > .MuiOutlinedInput-notchedOutline": {
      borderColor: IRON
    }
  }
})(FormControl);

const StyledSelect = withStyles({
  root: {
    fontFamily: "Overpass",
    color: SHUTTLE_GRAY,
    fontWeight: 200,
    "&.MuiSelect-select:focus": {
      background: "transparent"
    }
  },
  icon: {
    color: CERULEAN,
    fontSize: "1.8rem",
    top: "calc(50% - 14px)"
  }
})(Select);

const useStyles = makeStyles(() =>
  createStyles({
    inpurError: {
      border: "1px solid red"
    },
    labelError: {
      color: "red"
    },
    borderless: {
      "& > .MuiOutlinedInput-notchedOutline": {
        border: "none"
      }
    }
  })
);

SelectInput.defaultProps = {
  fullWidth: true,
  getValueToParent: () => {}
};

export default function SelectInput(props: DefaultSelectProps): JSX.Element {
  const [value, setValue] = React.useState(props.value);
  useEffect(() => {
    setValue(props.value ? props.value : "");
  }, [props.value]);

  const classes = useStyles();
  const field =
    props.errors !== undefined && props.errors.length > 0
      ? props.errors.filter((e: FormErrors) => e.path === props.name)
      : null;

  const handleChange = (event: React.ChangeEvent<{ value: any }>) => {
    setValue(event.target.value);
    props.getValueToParent(event.target.value);
  };

  return (
    <React.Fragment>
      <label className="default-select__label">{props.label}</label>
      <StyledFormControl variant="outlined" fullWidth={props.fullWidth}>
        {props.label && !value && (
          <InputLabel shrink={false} style={{ background: "white" }}>
            {props.placeholder}
          </InputLabel>
        )}
        <StyledSelect
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={value}
          onChange={handleChange}
          name={props.name}
          className={
            field && field[0]
              ? classes.inpurError
              : "" || props.borderless
              ? classes.borderless
              : ""
          }
        >
          <StyleMenuItem value="" disabled>
            {props.placeholder}
          </StyleMenuItem>
          {props.selectItems.map(
            (item: SelecItems, i: number): JSX.Element => (
              <StyleMenuItem key={i} value={item.value}>
                {item.label}
              </StyleMenuItem>
            )
          )}
        </StyledSelect>
      </StyledFormControl>
      {field && field[0] && (
        <label className={classes.labelError}>{field[0].errors}</label>
      )}
    </React.Fragment>
  );
}
