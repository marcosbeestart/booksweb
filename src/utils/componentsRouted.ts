import { RouteComponentProps } from "react-router";
import { RouteConfig } from "../routes";

interface ComponentRouted extends RouteConfig, RouteComponentProps {
  showLoading: any;
  hideLoading: any;
  authUser?: any;
}

export default ComponentRouted;
