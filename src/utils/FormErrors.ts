export interface FormErrors {
  path: string;
  errors: string[];
}
