interface DataProps {
  [key: string]: string;
}

export default function stringifyFormData(fd: any) {
  const data: DataProps = {};
  for (let key of fd.keys()) {
    data[`${key}`] = fd.get(key);
  }
  return JSON.stringify(data, null, 2);
}
