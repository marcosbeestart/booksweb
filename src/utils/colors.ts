/* Trying to use NameThatColor for colors :P -> http://chir.ag/projects/name-that-color/#FFC700; */

export const IRON = '#CBD1D4';
export const IRON_02 = 'rgba(203, 209, 212, 0.2)';
export const CERULEAN = '#0095DF';
export const CERULEAN_008 = 'rgba(0, 149, 223, 0.08)';
export const WHITE = '#FFFFFF';
export const SHUTTLE_GRAY = '#596266';
export const SUNSET_ORANGE = '#FF553E';
export const BLACK_HAZE = '#F5F7F7';
export const DAINTREE = '#001F2C';
export const OSLO_GRAY = '#828C90';
export const MYSTIC = '#E2E8EB';
export const SUPERNOVA = '#FFC700';
