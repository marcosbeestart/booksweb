import { rootRoute } from "../modules/root/routes";
import { booksRoutes } from "../modules/books/routes";

export interface RouteConfig {
  path: string;
  component: Function;
  routes?: RouteConfig[];
  exact?: boolean;
  p?: string;
  showLoading?: (message: string) => void;
  hideLoading?: () => void;
}

export const routes: RouteConfig[] = [...rootRoute, ...booksRoutes];
