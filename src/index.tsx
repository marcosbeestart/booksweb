import React from "react";
import ReactDOM from "react-dom";
import { ToastContainer } from "react-toastify";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { routes, RouteConfig } from "./routes";
import CloseIcon from "@material-ui/icons/Close";
import "./utils/_normalize.scss";
import "./utils/_index.scss";
import { withAuthentication } from "./database/withAuthentication";
import Loading from "./components/Loading/Loading";

export function RouteWithSubRoute(route: RouteConfig) {
  return (
    <Route
      path={route.path}
      exact={route.exact}
      render={(props) => {
        return (
          <route.component
            {...props}
            routes={route.routes}
            showLoading={route.showLoading}
            hideLoading={route.hideLoading}
          />
        );
      }}
    />
  );
}

const LoadingContext = React.createContext({
  loading: true,
  message: null,
  showLoading: (message: any) => {},
  hideLoading: () => {}
});

class AppComponent extends React.Component {
  state = {
    loading: false,
    message: null
  };

  showLoading = (message: any) =>
    this.setState({
      loading: true,
      message: null
    });

  hideLoading = () =>
    this.setState({
      loading: false
    });

  render() {
    const value = {
      ...this.state,
      showLoading: this.showLoading,
      hideLoading: this.hideLoading
    };
    return (
      <React.Fragment>
        <ToastContainer
          className="toast-container"
          autoClose={3000}
          closeButton={<CloseIcon />}
          toastClassName="toastify"
        />
        <LoadingContext.Provider value={value}>
          <LoadingContext.Consumer>
            {({ showLoading, hideLoading, loading }) => {
              return (
                <>
                  <Loading loading={loading} />
                  <Router>
                    <Switch>
                      {routes.map(
                        (route: RouteConfig, i: number): JSX.Element => {
                          return (
                            <RouteWithSubRoute
                              key={i}
                              {...route}
                              {...{ showLoading, hideLoading }}
                            />
                          );
                        }
                      )}
                    </Switch>
                  </Router>
                </>
              );
            }}
          </LoadingContext.Consumer>
        </LoadingContext.Provider>
      </React.Fragment>
    );
  }
}
export const App = withAuthentication(AppComponent);
ReactDOM.render(<App />, document.querySelector("#root"));
