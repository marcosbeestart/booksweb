import React, { useState, useEffect } from "react";
import Swal from "sweetalert2";
import ComponentRouted from "../../../utils/componentsRouted";
import Checkbox from "../../../components/Checkbox/DarkGrayCheckBox";
import {
  StyledTable,
  StyledTableCell,
  StyledTableCellHead,
  StyledTableRow,
  StyledTableHead,
  StyledTableBody
} from "../../../components/Table/Table";
import StyledSimpleTextButton from "../../../components/Button/SimpleTextButton";
import SelectAll from "../../../components/Button/SelecAll";
import { SHUTTLE_GRAY, CERULEAN } from "../../../utils/colors";
import FloatButton from "../../../components/Button/FloatButton";
import { deleteBook, getAllBooks } from "../../../database/books";
import _ from "lodash";
import { BookData, HeadRow } from "./interfaces";

interface Props {
  showLoading: any;
  hideLoading: any;
}

const headRows: HeadRow[] = [
  {
    id: "name",
    numeric: false,
    disablePadding: true,
    label: "Nome"
  },
  { id: "author", numeric: false, disablePadding: false, label: "Autor" },
  { id: "publisher", numeric: false, disablePadding: false, label: "Editora" },
  {
    id: "year",
    numeric: true,
    disablePadding: false,
    label: "Ano de Publicação"
  }
];

const Books = (props: ComponentRouted & Props): JSX.Element => {
  let [data, setData] = useState<BookData[]>([]);
  const [selected, setSelected] = useState<string[]>([]);
  const isSelected = (id: string) => selected.some((n) => n === id);

  useEffect(() => {
    getBooks();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getBooks = async () => {
    props.showLoading();

    data = await getAllBooks();
    setData(data);

    props.hideLoading();
  };

  function handleSelectAllClick(event: React.ChangeEvent<HTMLInputElement>) {
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n._id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  }

  function handleClick(event: React.MouseEvent<unknown>, id: string) {
    const selectedIndex = selected.indexOf(id);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  }

  function handleDeleteItems() {
    Swal.fire({
      title: "Tem certeza?",
      text: "Você não poderá reverter esta ação",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sim",
      cancelButtonText: "Cancelar"
    }).then(async (result) => {
      if (result.value) {
        props.showLoading();

        for (const bookId of selected) {
          await deleteBook(bookId);
        }
        await getBooks();

        props.hideLoading();
        
        Swal.fire("Pronto!", "O estabelecimento foi excluído", "success");
      }
    });
  }

  return (
    <React.Fragment>
      <div className="books">
        <div className="default-table">
          <div className="default-table__actions">
            <SelectAll onChange={handleSelectAllClick}>
              SELECIONAR TODOS
            </SelectAll>
            <div className="default-table__right-content">
              {selected.length > 0 && (
                <div>
                  <StyledSimpleTextButton
                    color={SHUTTLE_GRAY}
                    size={"14px"}
                    onClick={() => handleDeleteItems()}
                  >
                    <img src="/assets/icon-trash.svg" alt="Excluir" />
                    &nbsp;&nbsp; Excluir
                  </StyledSimpleTextButton>
                </div>
              )}
            </div>
          </div>

          <StyledTable>
            <StyledTableHead>
              <StyledTableRow>
                <StyledTableCellHead>&nbsp;</StyledTableCellHead>
                {headRows.map((row) => (
                  <StyledTableCellHead
                    key={row.id}
                    padding={row.disablePadding ? "none" : "default"}
                    colSpan={row.colspan}
                  >
                    {row.label}
                  </StyledTableCellHead>
                ))}
              </StyledTableRow>
            </StyledTableHead>
            <StyledTableBody>
              {data.map((row, index) => {
                const labelId = `enhanced-table-checkbox-${index}`;
                const isItemSelected = isSelected(row._id);

                return (
                  <StyledTableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={row.name}
                    selected={isItemSelected}
                  >
                    <StyledTableCell padding="checkbox">
                      <Checkbox
                        onClick={(event) => handleClick(event, row._id)}
                        checked={isItemSelected}
                        inputProps={{ "aria-labelledby": labelId }}
                      />
                    </StyledTableCell>
                    <StyledTableCell>{row.name}</StyledTableCell>
                    <StyledTableCell>{row.author}</StyledTableCell>
                    <StyledTableCell>{row.publisher}</StyledTableCell>
                    <StyledTableCell>{row.year}</StyledTableCell>
                    <StyledTableCell>
                      <StyledSimpleTextButton
                        size={"14px"}
                        color={CERULEAN}
                        onClick={() =>
                          props.history.push(`/books/edit/${row._id}`)
                        }
                      >
                        EDITAR
                      </StyledSimpleTextButton>
                    </StyledTableCell>
                    <StyledTableCell>
                      <button
                        className="normalized-button"
                        onClick={() => props.history.push(`book/${row._id}`)}
                      >
                        <img src="/assets/icon-arrow-right.svg" alt="Ver" />
                      </button>
                    </StyledTableCell>
                  </StyledTableRow>
                );
              })}
            </StyledTableBody>
          </StyledTable>
        </div>
      </div>
      <FloatButton onClick={() => props.history.push("/books/new")} />
    </React.Fragment>
  );
};
export default Books;
