export interface BookData {
  author: string;
  _id: string;
  name: string;
  publisher: string;
  year?: number;
}

export interface HeadRow {
  disablePadding: boolean;
  id: keyof BookData;
  label: string;
  numeric: boolean;
  colspan?: number;
  rowspan?: number;
}

export interface Plan {
  amount: number;
  charges: any;
  color: any;
  date_created: string;
  days: number;
  id: number;
  installments: number;
  invoice_reminder: any;
  name: string;
  object: string;
  payment_deadline_charges_interval: number;
  payment_methods: string[];
  trial_days: number;
}
