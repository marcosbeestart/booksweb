import React, { useState, useEffect } from "react";
import Breadcrumbs from "../../../components/Breadcrumbs/DefaultBreadcrumbs";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import TextInputMasked from "../../../components/TextInput/TextInputMasked";
import TextInput from "../../../components/TextInput/TextInput";
import Grid from "@material-ui/core/Grid";
import ComponentRouted from "../../../utils/componentsRouted";
import DefaultButton, {
  DefaultButtonOutlined
} from "../../../components/Button/DefaultButton";
import stringifyFormData from "../../../utils/stringifyFormDAta";
import { FormErrors } from "../../../utils/FormErrors";
import "./book.scss";
import { getBook, updateBook, createBook } from "../../../database/books";
import { BookData } from "./interfaces";
import { bookSchema } from "./schemas";

import Toast from "../../../components/Toast/Toast";

interface Props {
  showLoading: any;
  hideLoading: any;
}
export default function Book(
  props: ComponentRouted & Props
): JSX.Element | null {
  let [data, setData] = useState<BookData>();
  const initialStateErrors = [
    {
      path: "",
      errors: []
    }
  ];
  const [errors, setFormErrors] = React.useState<FormErrors[]>(
    initialStateErrors
  );

  useEffect(() => {
    const getData = async () => {
      props.showLoading();
      if ("id" in props.match.params) {
        // editando
        const id = props.match.params["id"];
        data = await getBook(id);
        setData(data);
      } else {
        // criando
        setData({ _id: "", name: "", author: "", publisher: "" });
      }
      props.hideLoading();
    };
    getData();
    // eslint-disable-next-line
  }, []);

  const handleSubmit = (event: any): void => {
    event.preventDefault();

    const formData = new FormData(event.target);
    const formated = stringifyFormData(formData);

    bookSchema
      .validate(formated, { abortEarly: false })
      .then(async (valid) => {
        props.showLoading("aqui valid", valid);
        if ("id" in props.match.params) {
          // editando
          const { id } = props.match.params;
          const book: BookData = {
            ...valid,
            year: Number(valid.year),
            _id: id
          };
          await updateBook(id, book);
        } else {
          // criando
          await createBook({
            ...valid,
            year: Number(valid.year)
          } as BookData);
        }

        Toast("success", "Livro salvo com sucesso!");
        props.hideLoading();
        props.history.goBack();
        setFormErrors(initialStateErrors);
      })
      .catch((err) => {
        props.hideLoading();

        console.log("schema error:", err);
        if (err && err.inner) {
          setFormErrors(
            err.inner.map((e: FormErrors) => {
              return {
                path: e.path,
                errors: e.errors
              };
            })
          );
        } else {
          Toast(
            "error",
            "Erro ao atualizar estabelecimento. Por favor tente novamente!"
          );
        }
      });
  };

  return data ? (
    <div className="book">
      <div className="book__content">
        <h2 className="pages-title-h2">{data.name} </h2>
        <Breadcrumbs>
          <Link color="inherit" href="/books">
            Livros
          </Link>
          <Typography>Editar livro</Typography>
        </Breadcrumbs>

        <form onSubmit={handleSubmit}>
          <div className="book__form-content">
            <h3 className="pages-title-h3">Dados</h3>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <TextInput
                  fullWidth
                  placeholder="Digite o nome do livro"
                  label="Nome"
                  value={data.name}
                  errors={errors}
                  name="name"
                />
              </Grid>
              <Grid item xs={6}>
                <TextInput
                  fullWidth
                  placeholder="Digite o nome do autor"
                  label="Autor"
                  value={data.author}
                  errors={errors}
                  name="author"
                />
              </Grid>

              <Grid item xs={6}>
                <TextInput
                  fullWidth
                  placeholder="Digite o nome da editora"
                  label="Editora"
                  value={data.publisher}
                  errors={errors}
                  name="publisher"
                />
              </Grid>

              <Grid item xs={6}>
                <TextInputMasked
                  fullWidth
                  placeholder="2000"
                  label="Ano"
                  value={String(data.year)}
                  errors={errors}
                  name="year"
                  mask="9999"
                />
              </Grid>
            </Grid>
          </div>

          <div className="books__button">
            <DefaultButtonOutlined
              width="282px"
              onClick={() => props.history.goBack()}
            >
              Cancelar
            </DefaultButtonOutlined>
            <DefaultButton width="282px" type="submit">
              Salvar
            </DefaultButton>
          </div>
        </form>
      </div>
    </div>
  ) : null;
}
