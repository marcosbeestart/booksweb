import * as yup from "yup";
import { StringSchema } from "yup";
import { isValid } from "@fnando/cnpj";

declare module "yup" {
  interface StringSchema {
    cnpj(): StringSchema;
  }
}

yup.addMethod<StringSchema>(yup.string, "cnpj", function() {
  return this.test("cnpj", "CNPJ inválido", function(value) {
    if (value) {
      const cnpj = isValid(value);
      if (cnpj) return true;
    }
    return false;
  });
});

export const bookSchema = yup.object().shape({
  name: yup.string().required("Campo é obrigatório"),
  publisher: yup.string().required("Campo é obrigatório"),
  author: yup.string().required("Campo é obrigatório"),
  year: yup.string().required("Campo é obrigatório")
});
