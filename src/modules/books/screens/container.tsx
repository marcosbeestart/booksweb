import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { RouteWithSubRoute } from '../../../index';
import { RouteConfig } from '../../../routes';

export default function Container(props: any): JSX.Element {
  return (
    <>
      <Switch>
        {props.routes.map((route: RouteConfig, i: number) => {
          return (
            <RouteWithSubRoute
              key={i}
              {...route}
              showLoading={props.showLoading}
              hideLoading={props.hideLoading}
            />
          );
        })}
        <Route
          path={`/books`}
          render={() => <Redirect to={`/books/all`} />}
          exact
        />
      </Switch>
    </>
  );
}
