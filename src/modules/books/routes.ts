import { RouteConfig } from "../../routes";
import AllBooks from "./screens/all";
import Container from "./screens/container";
import CreateOrEditBook from "./screens/createOrEdit";
import { withAuthorization } from "../../database/withAuthorization";

const authCondition = (authUser: any) => !!authUser;
const path = "/books";

export const booksRoutes: RouteConfig[] = [
  {
    path: `${path}`,
    component: withAuthorization(authCondition)(Container),
    routes: [
      {
        path: `${path}/all`,
        component: withAuthorization(authCondition)(AllBooks)
      },
      {
        path: `${path}/edit/:id`,
        component: withAuthorization(authCondition)(CreateOrEditBook)
      },
      {
        path: `${path}/new`,
        component: withAuthorization(authCondition)(CreateOrEditBook)
      }
    ]
  }
];
