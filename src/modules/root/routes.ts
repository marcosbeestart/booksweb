import { RouteConfig } from "../../routes";
import { withAuthorization } from "../../database/withAuthorization";
import AllBooks from "../books/screens/all";

export const rootRoute: RouteConfig[] = [
  {
    path: "/",
    component: withAuthorization(true)(AllBooks),
    exact: true
  }
];
